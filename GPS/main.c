#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <setjmp.h>

#define QTE 15

typedef char MY_TYPE;

typedef struct {
   char heure[3]; //06
   char minute[3]; //40
   char secondes[3]; //36
   char centiemes[5];//.289
} Temps;

typedef struct {
   char latitude[11];
   char longitude[12];
} Trame;

typedef enum {OK,PAS_GPGGA,CHAMP_INVALIDE,PTR_NULL} Exception;

//Recupere une sous-chaine d'une chaine ch de l'indice debut+orign jusqu'� n
char * retrievesubstr(char *ch,const char *orign,int debut, int n){
   strncpy(ch,(orign+debut),n);
   ch[n]='\0';//Neutralisation de la fin de chaine
   return ch;
}

//Indique si la chaine ch pass�e en param�tre est une trame GPGGA
//Retourne 1 si ch est une trame GPGGA
//L�ve PAS_GPGGA
int estGGPA(char ch[],jmp_buf ptRep){
   if(strstr(ch,"$GPGGA")!=NULL && strlen(ch)>15){
      return 1;
   } else {
      longjmp(ptRep, PAS_GPGGA);
   }
}

char * convertirLonLat(char * ch, jmp_buf ptRep){

   if(ch==NULL){
      longjmp(ptRep,PTR_NULL);
      }

   double p = atof(ch);
   int degre;
   int minute;
   double secon;

   degre = (int)(p / 100.0);
   minute = (int)(p - (degre * 100.0));
   secon = 60.0 * (p - (degre * 100.0) - minute);

   char * str = calloc(14,sizeof(char));

   sprintf(str,"%d�%d'%.*f''",degre,minute,2,secon);
   return str;
}

char * formaterHeure(Temps * tem, jmp_buf ptRep){

   if(tem==NULL){
      longjmp(ptRep,PTR_NULL);
     }

   char * str = calloc(14,sizeof(char));

   sprintf(str,"%sh%sm%ss",tem->heure,tem->minute,tem->secondes);

   return str;

}

void affichageHeure(Temps * tem, jmp_buf ptRep){

   char * ch = formaterHeure(tem,ptRep);
   printf("HEURE : %s\n",ch);

}

void affichageLonLat(Trame * trm, jmp_buf ptRep){
   printf("Latitude : %s\n",convertirLonLat(trm->latitude,ptRep));
   printf("Longitude : %s\n",convertirLonLat(trm->longitude,ptRep));
}


void recupererInfo(char * trm,Trame * trame, Temps * temps, jmp_buf ptRep){
   char liste[20][20];
   char a ='a';
   char * firstOcc= &a ;
   char test[55]="";
   char * testP= test;
   char * ptrm = trm;
   char virgule[]=",";
   firstOcc = strstr(trm,virgule);

   for(int i=0;i<4;i++){
      ptrm = firstOcc+strlen(virgule);

      testP = firstOcc;
      firstOcc = strstr(ptrm,virgule);
      retrievesubstr(test,trm,testP-trm+1,firstOcc-testP-1);
      for(int j=0;j<strlen(test);j++){
	 liste[i][j]=test[j];
      }
   }
   liste[0][10]='\0'; //Neutralisation du bruit


   strncpy(trame->latitude,liste[1],9);
   strncpy(trame->longitude,liste[3],10);

   for(int i=0;i<2;i++){
      temps->heure[i]=liste[0][i];
   }
   temps->heure[2]='\0';
   for(int i=2;i<4;i++){
      temps->minute[i-2]=liste[0][i];
   }
   temps->minute[2]='\0';
   for(int i=4;i<6;i++){
      temps->secondes[i-4]=liste[0][i];
   }
   temps->secondes[2]='\0';
   for(int i=7;i<10;i++){
      temps->centiemes[i-7]=liste[0][i];
   }


}

int main() {
   jmp_buf ptRep;
   Exception erreur;
   erreur=setjmp(ptRep);
   char ch[] = "$GPGGA,064036.289,4836.5375,N,00740.9373,E,1,04,3.2,200.2,M,,,,0000*0E";
   char trm[50] ;
   strncpy(trm,ch,42);
   strcat(trm, "\0");
   printf("Info a sortir : %s\n",trm);
   printf("Taille en memoire d'une trame : %d\n",(int)sizeof(ch));
   Temps temps;
   Temps * pTemps=&temps;
   Trame trame;
   Trame * pTrame=&trame;
   recupererInfo(trm,pTrame,pTemps,ptRep);
   affichageLonLat(pTrame,ptRep);
   affichageHeure(pTemps,ptRep);
}
