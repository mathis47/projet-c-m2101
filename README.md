# Projet C M2101

Projet de M2101 des étudiants Samuel De Amoedo Amorim et Kilyan Le Gallic portant
sur le traitement de trames GPS NMEA format GPGGA en utilisant le langage C

```c
int vérifierTrame (char ch[],jmp_buf ptRep);
Erreurs possibles: TRAME_TROP_COURTE, TRAME_PAS_GPGGA
```

```c
void extraireChamps(char ch, Trame * trame , Temps * temps,jmp_buf ptRep);

Erreurs possibles: PTR_NUL, OUT_OF_BOND
(Nécessité de la fonction retrievesubstr)
(char * ch, const char *orign, int debut, int n){
	strncpy(ch,(orign+debut),n);
	return ch;
}
```

```c
void convertirLonLat(Trame * trm, jmp_buf ptRep);
Erreurs possibles: ILLEGAL_ARGUMENT, OUT_OF_BOND
```
```c
void formaterHeure( Temps * tem, jmp_buf ptRep);
Erreurs possibles: ILLEGAL_ARGUMENT, OUT_OF_BOND
```

```c
void affichageHeure(Temps tem,jmp_buffer ptRep);
Erreurs possibles: PAS_CHAR, OUT_OF_BOND
```
```c
void affichageLonLat(Trame trm,jmp_buffer ptRep);
Erreurs possibles: PAS_CHAR, OUT_OF_BOND
```